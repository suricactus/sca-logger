import createLogger from './index';

const logger = createLogger({ name: 'New name' });

test('Should be function', () => {
  expect(createLogger).toBeInstanceOf(Function);
  expect(logger.info).toBeInstanceOf(Function);

  try {
    throw new Error('test err');
  } catch (error) {
    logger.error(error);
    logger.info('Something else', { other: 'data' });
  }
  logger.info('test message %s, %s', 'first', 'second', { number: 123 });
});
