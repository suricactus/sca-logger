# sca-logger

Logger with pretty good default and support for `node-config` configuration.

It uses the wonderful `node-bunyan`.

## Configuration

- `name` "\<NO NAME\>" - logger name, used as prefix of all log messages

- `logrotate` true - is log rotate enabled

- `logrotatePeriod` '1d' - period for logrotate

- `logrotateCount` 7 - how many files to store

- `logRootPath` '.' - where to write log files

- `disableStderrStream` false - disables writing human readable log to STDERR

- `streams` - array of objects, containing `node-bunyan` type of configuration about each stream

```
  streams: [{
    level: 'info',
    type: 'rotating-file',
    period: '1d',
    count: 3,
    stream: 'logs/info.log',
  }, {
    level: 'error',
    stream: 'logs/error.log',
  }],
```