import config from 'config';
import bunyan from 'bunyan';
import bunyanFormat from 'bunyan-format';

const formatOut = bunyanFormat({ outputMode: 'short' }, process.stderr);

export const createLogger = ({
  name,
  disableStderrStream,
  streams,
}) => {
  const stderrStream = {
    level: 'debug',
    stream: formatOut,
  };

  streams = streams && streams.length
    ? [stderrStream]
    : disableStderrStream
      ? streams
      : [stderrStream, ...streams];

  const logger = bunyan.createLogger({
    name,
    streams,
  });

  return logger;
};

const defaults = {
  name: '<NO NAME>',
  logrotate: true,
  logrotatePeriod: '1d',
  logrotateCount: 7,
  logRootPath: '.',
  disableStderrStream: false,
  streams: [{
    level: 'info',
    type: 'file',
    stream: 'logs/info.log',
  }, {
    level: 'error',
    type: 'file',
    stream: 'logs/error.log',
  }],
};

// Mixin configs that have been passed in, and make those my defaults
const settings = config.has('logger') ? config.util.toObject(config.get('logger')) : {};

config.util.extendDeep(defaults, settings);

for (const stream of Object.values(defaults.streams)) {
  if (defaults.streams) {
    if (stream.type !== 'file') continue;

    stream.type = 'rotating-file';
    stream.period = defaults.logrotatePeriod;
    stream.count = defaults.logrotateCount;
    stream.stream = typeof stream.stream === 'string'
      ? defaults.logRootPath + stream.stream
      : stream.stream;
  }
}

config.util.setModuleDefaults('logger', defaults);

const logger = createLogger({
  name: config.get('logger.name'),
  disableStderrStream: config.get('logger.disableStderrStream'),
  streams: config.get('logger.streams'),
});

export default logger;
